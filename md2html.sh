#!/bin/sh

docker run \
    --rm \
    --name md2html \
    --volume "$( pwd ):/data" \
    --user $( id -u ):$( id -g ) \
    letompouce/md2html:buster-slim \
    "$@"
