FROM debian:buster-slim

# hadolint ignore=DL3008
RUN apt-get update \
        && apt-get install --yes --no-install-recommends --quiet --quiet \
            pandoc \
            tidy \
        && apt-get clean \
        && rm -rf /var/lib/apt/lists/*

WORKDIR /data
